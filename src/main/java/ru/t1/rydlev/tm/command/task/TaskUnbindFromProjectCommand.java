package ru.t1.rydlev.tm.command.task;

import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT iD:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

}
