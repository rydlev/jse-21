package ru.t1.rydlev.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + getPropertyService().getApplicationName());

        System.out.println("[DEVELOPER]");
        System.out.println("NAME: " + getPropertyService().getAuthorName());
        System.out.println("E-MAIL: " + getPropertyService().getAuthorEmail());

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + getPropertyService().getGitBranch());
        System.out.println("COMMIT ID: " + getPropertyService().getGitCommitId());
        System.out.println("COMMITER: " + getPropertyService().getGitCommitterName());
        System.out.println("E-MAIL: " + getPropertyService().getGitCommitterEmail());
        System.out.println("MESSAGE: " + getPropertyService().getGitCommitMessage());
        System.out.println("TIME: " + getPropertyService().getGitCommitTime());
    }

    @Override
    public String getArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public String getName() {
        return "info";
    }

}
