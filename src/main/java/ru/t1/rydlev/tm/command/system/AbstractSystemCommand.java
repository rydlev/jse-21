package ru.t1.rydlev.tm.command.system;

import ru.t1.rydlev.tm.api.service.ICommandService;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
