package ru.t1.rydlev.tm.api.model;

import ru.t1.rydlev.tm.enumerated.Role;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    Role[] getRoles();

    void execute();

}
