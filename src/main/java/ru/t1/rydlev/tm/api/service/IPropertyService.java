package ru.t1.rydlev.tm.api.service;

import ru.t1.rydlev.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    String getApplicationVersion();

    String getApplicationConfig();

    String getApplicationName();

    String getAuthorEmail();

    String getAuthorName();

    String getGitBranch();

    String getGitCommitId();

    String getGitCommitterName();

    String getGitCommitterEmail();

    String getGitCommitMessage();

    String getGitCommitTime();

}
