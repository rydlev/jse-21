package ru.t1.rydlev.tm.exception.system;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! Permission denied...");
    }

}
