package ru.t1.rydlev.tm.service;

import ru.t1.rydlev.tm.api.repository.IProjectRepository;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.service.IProjectTaskService;
import ru.t1.rydlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rydlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.rydlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.rydlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.forEach(t -> taskRepository.removeById(userId, t.getId()));
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void removeProjects() {
        final List<Project> projects = projectRepository.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            removeProjectById(project.getUserId(), project.getId());
        }
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}
