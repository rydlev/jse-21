package ru.t1.rydlev.tm.repository;

import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    private Predicate<Task> filterByProjectId(final String projectId) {
        return m -> projectId.equals(m.getProjectId());
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .filter(filterByProjectId(projectId))
                .collect(Collectors.toList());
    }

}
